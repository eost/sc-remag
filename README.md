# SCREMAG

`scremag` is a seiscomp3 application which aims to reprocess Magnitudes,
StationMagnitudes, StationMagnitudeContributions and Amplitudes. It basically
just sends an `UPDATE` message and let `scmag` recalculate new magnitudes.

## Installation

1. Copy the different files into seiscomp3 folders:

    Synchronise the source into seiscomp3 folder:

    ```bash
    rsync -av seiscomp3/ /path/to/seiscomp3/
    ```

2. Configure `scremag` (optional, the messages are send to the messaging group
   `LOCATION` by default)

3. Use it:

    You can use the `-E` option to reprocess magnitudes of the preferred origin of
    an event or the `-O` option to a specific origin:

    ```bash
    seiscomp exec scremag -E event_id
    seiscomp exec scremag -O origin_id
    ```

    > **Warning**
    >
    > With the `-E` option, the magnitudes of the preferred origin are reprocessed
    > but `scevent` can change the preferred origin after this reprocessing. The
    > new preferred origin will not have reprocessed magnitudes.

    Or specify a time window:

    ```bash
    seiscomp exec scremag --begin 2016-03-01T00:00:00.0 --end 2016-03-08T00:00:00.0
    ```

    It is also possible to remove the Magnitudes, StationMagnitudes,
    StationMagnitudeContributions and Amplitudes of an Origin before reprocessing
    them:

    ```bash
    # Remove Magnitudes, StationMagnitudes and StationMagnitudeContributions
    seiscomp exec scremag -O origin_id --remove-magnitudes Md,Mlv
    # Remove Amplitudes
    seiscomp exec scremag -O origin_id --remove-amplitudes Md,Mlv
    # Remove all
    seiscomp exec scremag -O origin_id -R
    ```

    > **Warning**
    >
    > Without these options, the Amplitudes aren't reprocessed. Only new Amplitudes
    > not yet existing will be added.

## Known issue

Sometimes `scmag` doesn't recompute a magnitude due to a
[cache issue](https://github.com/SeisComP3/seiscomp3/issues/178). `scmag`
should  work properly after a restart.
